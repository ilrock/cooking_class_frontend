// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './components/App.vue'
import router from './routes.js'
import store from './store.js'
import Auth from './plugins/Auth.js'

window.axios = require('axios')
axios.defaults.baseURL = "http://localhost:3000/api/v1";

// Set the JWT token if it exists
axios.interceptors.request.use((config) =>{
	if (Vue.auth.getToken()){
    	config.headers['Authorization'] = "Bearer " + Vue.auth.getToken();
	}
	return config;
});

Vue.use(Auth);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
});
