import Vue from 'vue';
import Vuex from 'vuex';
import Auth from './plugins/Auth.js'

Vue.use(Vuex);
Vue.use(Auth);

export default new Vuex.Store({
	state: {
		currentUser: {},
		isLoggedIn: false
	},
	mutations: {
		setCurrentUser: function (state, user){
			state.currentUser = user;
		},
		addToCurrentUser: function(state, prop, value){
			Vue.set(state.currentUser, prop, value);
		},
		clearCurrentUser: function(state){
			state.currentUser = {};
		},
		setIsLoggedIn: function(state, isLoggedIn){
			state.isLoggedIn = isLoggedIn;
		},
		clearIsLoggedIn: function(state){
			state.isLoggedIn = false;
		}
	}
});
